package com.yourcareuniverse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yourcareuniverse.model.TimeslotProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class InitSlots {

    private static final String DEFAULT_PRACTICE_ID = "195900";
    private static Duration period = Duration.ofHours(1);
    private static RestTemplate rs = new RestTemplate();

    private static HttpEntity<?> initRequest(String body, String token) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("Authorization", "Bearer " + token);
        params.add("Content-Type", "application/x-www-form-urlencoded");
        HttpEntity<?> ent = new HttpEntity<>(body, params);
        return ent;
    }

    private static String initTimes(LocalDate date) {
        ArrayList<String> times = new ArrayList<>();
        LocalDateTime dt = date.atTime(0, 0);
        while ((dt = dt.plus(period)).toLocalDate().isEqual(date)) {
            times.add(dt.toLocalTime().format(DateTimeFormatter.ofPattern("kk:mm")));
        }
        return times.stream().collect(Collectors.joining(","));
    }

    private static String initBody(String typeId, String departmentId, String providerId, LocalDate date)
            throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        sb.append("appointmentdate=");
        sb.append(URLEncoder.encode(date.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), "UTF8"));
        sb.append("&");
        sb.append("appointmenttime=");
        sb.append(URLEncoder.encode(initTimes(date), "UTF8"));
        sb.append("&");
        sb.append("appointmenttypeid=");
        sb.append(URLEncoder.encode(typeId, "UTF8"));
        sb.append("&departmentid=");
        sb.append(URLEncoder.encode(departmentId, "UTF8"));
        sb.append("&");
        sb.append("providerid=");
        sb.append(URLEncoder.encode(providerId, "UTF8"));
        return sb.toString();
    }

    private static void initSlots(String typeId, String practiceId, String departmentId, String providerId, LocalDate start, LocalDate end, String token)
            throws UnsupportedEncodingException, URISyntaxException {
        if (practiceId == null ||  practiceId.isEmpty()) {
            practiceId = DEFAULT_PRACTICE_ID;
        }
        URI uri = new URI("https://api.athenahealth.com/preview1/" + practiceId +"/appointments/open");
        while (!end.isBefore(start)) {
            String body = initBody(typeId, departmentId, providerId, start);
            System.out.println(body);
            System.out.println(URLDecoder.decode(body, "UTF8"));
            String res = rs.exchange(uri, HttpMethod.POST, initRequest(body, token), String.class).getBody();
            System.out.println(res);
            System.out.println("---------------------------------------------------------------");
            start = start.plusDays(1);
        }
    }

    private static void initSlots(TimeslotProperties properties) throws Exception {
        for (String provider : properties.getProviderIds()) {
            for (String type : properties.getAppointmentTypeIds()) {
                initSlots(type, properties.getPracticeId(), properties.getDepartmentId(), provider,
                        properties.getStart(), properties.getEnd(), properties.getToken());
            }
        }
    }

    private static TimeslotProperties getProperties(String filepath) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TimeslotProperties properties = mapper.readValue(new FileInputStream(filepath), TimeslotProperties.class);
        return properties;
    }

    public static void main(String... args) throws Exception {
        TimeslotProperties properties = getProperties("timeslots-properties.json");
        initSlots(properties);
    }

}
