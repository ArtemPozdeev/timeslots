package com.yourcareuniverse.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.yourcareuniverse.json.LocalDateDeserializer;

import java.time.LocalDate;

public class TimeslotProperties {
    private String[] providerIds;
    private String[] appointmentTypeIds;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate start;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate end;
    private String token;
    private String departmentId;
    private String practiceId;

    public String[] getProviderIds() {
        return providerIds;
    }

    public void setProviderIds(String[] providerIds) {
        this.providerIds = providerIds;
    }

    public String[] getAppointmentTypeIds() {
        return appointmentTypeIds;
    }

    public void setAppointmentTypeIds(String[] appointmentTypeIds) {
        this.appointmentTypeIds = appointmentTypeIds;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(String practiceId) {
        this.practiceId = practiceId;
    }
}
